package com.maksimLarchanka;

import java.io.*;
import java.util.*;

import static com.maksimLarchanka.Employee.readEmployee;
import static com.maksimLarchanka.Employee.writeEmployee;

public class Main
{
    public static void main(String[] args) throws IOException
    {
        Employee[] staff = new Employee[3];

        staff[0] = new Employee("Carl Cracker", 75000);
        staff[1] = new Employee("Harry Hacker", 50000);
        staff[2] = new Employee("Tony Tester", 40000);


        try (PrintWriter out = new PrintWriter("employee.dat", "UTF-8"))
        {
            writeData(staff, out);
        }
        try (Scanner in = new Scanner(
                new FileInputStream("employee.dat"), "UTF-8"))
        {
            Employee[] newStaff = readData(in);
            for (Employee e : newStaff)
                System.out.println(e);
        }
        catch (IOException exeption){
            exeption.printStackTrace();
        }
    }
    private static void writeData(Employee[] employees, PrintWriter out) throws IOException
    {
        out.println(employees.length);
        for (Employee e : employees)
            writeEmployee(out, e);
    }

    private static Employee[] readData(Scanner in)
    {
        int n = in.nextInt();
        in.nextLine();
        Employee[] employees = new Employee[n];
        for (int i = 0; i < n; i++)
        {
            employees[i] = readEmployee(in);
        }
        return employees;
    }

}
