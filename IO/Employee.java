package com.maksimLarchanka;

import java.io.PrintWriter;
import java.util.*;

public class Employee
{
    private String name;
    private double salary;
    private Date hireDay;

    public Employee(String n, double s)
    {
        name = n;
        salary = s;
    }

    public String getName()
    {
        return name;
    }

    public double getSalary()
    {
        return salary;
    }

    public String toString()
    {
        return getClass().getName() + "[name=" + name + ",salary=" + salary +"]";
    }

    public static void writeEmployee(PrintWriter out, Employee e)
    {
        out.println(e.getName() + "|" + e.getSalary());
    }
    public static Employee readEmployee(Scanner in)
    {
        String line = in.nextLine();
        String[] tokens = line.split("\\|");
        String name = tokens[0];
        double salary = Double.parseDouble(tokens[1]);
        return new Employee(name, salary);
    }
}
